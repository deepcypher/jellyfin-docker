#!/usr/bin/env bash
sudo docker build -t a/jellyfin -f Dockerfile . && \
sudo docker run -p 127.0.0.1:8096:8096 -it a/jellyfin
