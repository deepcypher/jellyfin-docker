FROM jellyfin/jellyfin

ARG USERNAME="archer"

RUN useradd ${USERNAME} && \
    chown -R ${USERNAME}:${USERNAME} /jellyfin /config /cache 

USER ${USERNAME}

ENV HEALTHCHECK_URL=http://localhost:8096/health

EXPOSE 8096
VOLUME /cache /config
ENTRYPOINT ["./jellyfin/jellyfin", \
    "--datadir", "/config", \
    "--cachedir", "/cache", \
    "--ffmpeg", "/usr/lib/jellyfin-ffmpeg/ffmpeg"]

HEALTHCHECK --interval=30s --timeout=30s --start-period=10s --retries=3 \
     CMD curl -Lk "${HEALTHCHECK_URL}" || exit 1
