#!/usr/bin/env sh
# VERSION="10.7.7"
VERSION=$1
ATYPE=$(uname -m)
BASE="https://repo.jellyfin.org/releases/server/linux/stable/combined/jellyfin_${VERSION}_"

case "${ATYPE}" in
  arm*) ATYPE="armhf"
    URL="${BASE}${ATYPE}.tar.gz"
    ;;
  aarch64) ATYPE="arm64"
    URL="${BASE}${ATYPE}.tar.gz"
    ;;
  x86_64) ATYPE="amd64"
    URL="${BASE}${ATYPE}.tar.gz"
    ;;
esac

echo ${URL}
